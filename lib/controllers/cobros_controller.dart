import 'package:get/state_manager.dart';

class CobrosController extends GetxController {
  var saldo = 0.obs;
  var data = {"complete": 0, "saldo": 0, "mora": 0}.obs;

  var tipoCredito = "".obs;
  var saldoredito = "".obs;
  var vCuotaMensual = "".obs;
  var cuotasRestantes = "".obs;
  var diasMora = "".obs;
  var interesMora = "".obs;

  CobrosController();

  validar() {
    print('validar');
    this.tipoCredito.value = "MicroCredito";
    print(tipoCredito.value);
    this.saldoredito.value = "S-12783";
    this.vCuotaMensual.value = "S-12367";
    this.cuotasRestantes.value = "16";
    this.cuotasRestantes.value = "5";
    this.diasMora.value = "3";
    this.interesMora.value = "10%";
  }

  reset() {
    this.tipoCredito.value = "";
    this.saldoredito.value = "";
    this.vCuotaMensual.value = "";
    this.cuotasRestantes.value = "";
    this.cuotasRestantes.value = "";
    this.diasMora.value = "";
    this.interesMora.value = "";
  }

  //Obtener el saldo de la cuenta del socio
  getBalance() {
    // AQUI CONECTAR CON EL BUS
    return ({
      'balance': 156.78,
      'ci': '0503646556',
    });
  }

  getCreditInfo(nCuenta) {
    // Aqui se conecta con la nube
    return ({
      "type": "MICROCREDITO",
      "balance": 7000,
      'pay': 356,
      "mora": 3,
      'interes_mora': 0.15
    });
  }

  fillData() {
    data.value["complete"] = 1;
    data.value["saldo"] = 10;
    data.value["mora"] = 5;
  }
}
