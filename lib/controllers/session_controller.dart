import 'dart:convert';

import 'package:get/state_manager.dart';
import 'package:http/http.dart' as http;

class SessionController extends GetxController {
  var token = "".obs;
  var name = "".obs;
  var email = "".obs;
  var phone = "".obs;
  var userName = "".obs;
  var userId = "".obs;
  SessionController();

  Future<void> loginConection(
      {required String name, required String password}) async {
    print("INICIO DE CONEXION");

    final url = Uri.parse('http://35.193.96.121/services/login-cobranzas');
    print("URL VALIDO");

    var response =
        await http.post(url, body: ({'name': name, 'password': password}));

    print("ENTRA AL POST");
    String body = utf8.decode(response.bodyBytes);
    print('BODY ==========$body');
    final jsonData = jsonDecode(body);

    // if (response.statusCode == 200) {
    print('ESTADO ===${response.statusCode}');
    // print(jsonData['status']);
    if (jsonData['userId'] == 'LUIS') {
      print("---CONEXION CORRECTA---");
      this.token.value = jsonData['token'];
      this.name.value = jsonData['name'];
      this.email.value = jsonData['email'];
      this.phone.value = jsonData['phone'];
      this.userName.value = jsonData['userName'];
      this.userId.value = jsonData['userId'];
      print("nombre: $name");
      }
      // } else {
    //   print("NO HAY CONEXION");
    // }
  }
}

