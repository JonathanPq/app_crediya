import 'package:flutter/material.dart';
import 'package:template_credi/pages/operation_pages/cobros_page.dart';
import 'package:template_credi/pages/operation_pages/depositos_page.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/tarjeta_actividad_nueva.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class ActividadPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Medida de la pantalla
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              // Logo
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),

              // Encabezado
              TextoW(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.9,
                label: '¿Qué deseas realizar?',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),

              // Tarjeta - Depositos
              TarjetaActividadNueva(
                assetLogoImage: 'assets/images/deposito_blanco.png',
                cardColor: Color(0xFFD20B12),
                label: 'Depósitos',
                labelColor: Colors.white,
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => DepositosPage()));
                },
              ),
              SizedBox(height: size.height * 0.02),
              // Tarjeta - Cobros
              TarjetaActividadNueva(
                assetLogoImage: 'assets/images/cobro_gris.png',
                cardColor: Colors.white,
                label: 'Cobros',
                labelColor: Color(0xFF3F3F3F),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => CobrosPage()));
                },
              ),
              SizedBox(height: size.height * 0.05),
            ],
          ),
        ),
      ),
    );
  }
}
