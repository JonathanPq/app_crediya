import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template_credi/controllers/cobros_controller.dart';
import 'package:template_credi/pages/confirmation_pages/cobro_exitoso.dart';
import 'package:template_credi/pages/confirmation_pages/confirmar_cobro.dart';
import 'package:template_credi/widgets/datos_cobros.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/text_form_field.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class CobrosPage extends StatelessWidget {
  //_____________--

  //_____________--

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // final CobrosController datosController = CobrosController();
    final CobrosController getXCobros = Get.put(CobrosController());
    //validar

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              //Logo Crediya
              // Texto
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Informacion de socio',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              //Numero de cuenta y boton validar
              Container(
                width: size.width * 0.85,
                height: size.height * 0.1,
                child: Row(
                  children: <Widget>[
                    //Input de No. Cuenta
                    Expanded(
                      child: textFromField(
                        sizeH: size.height * 0.08,
                        sizeW: size.width * 0.85,
                        label: "No. Cuenta",
                        autocorrect: false,
                        inputType: TextInputType.number,
                        password: false,
                      ),
                    ),
                    SizedBox(width: size.width * 0.02),

                    //Boton validar
                    MyBotton(
                      label: 'Validar',
                      labelColor: Colors.white,
                      labelSize: 20,
                      bottonColor: Color(0xFFD20B12),
                      sizeW: size.width * 0.30,
                      sizeH: size.height * 0.08,
                      fontWeight: FontWeight.bold,
                      ontap: () {
                        // c.fillData();
                        // print(c.data.toString());
                        getXCobros.validar();
                      },
                    ),
                  ],
                ),
              ),
              MyBotton( 
                label: 'No Validar',
                labelColor: Colors.white,
                labelSize: 20,
                bottonColor: Color(0xFFD20B12),
                sizeW: size.width * 0.30,
                sizeH: size.height * 0.08,
                fontWeight: FontWeight.bold,
                ontap: () {
                  // c.fillData();
                  // print(c.data.toString());
                  getXCobros.reset();
                },
              ),
              //Pantalla GetX
              TextoW(
                sizeH: size.height * 0.15,
                sizeW: size.width * 0.9,
                label: 'Información de crédito',
                labelSize: 28,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              // Informacion de credito
              //Tipo C.
              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Tipo de credito:',
              //   // labelDescription: tipoCredito.toString(),
              //   labelDescription: c.texto.toString(),

              //   fontSize: 16,
              // ),
              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Tipo de credito',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.tipoCredito.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),
              //Saldo C.
              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Saldo de credito:',
              //   labelDescription: 'S-1823',
              //   fontSize: 16,
              // ),
              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Saldo de credito',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.saldoredito.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),

              //Valor C.

              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Valor de cuota mensual:',
              //   labelDescription: 'S-18123',
              //   fontSize: 16,
              // ),
              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Valor cuota mensual:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.vCuotaMensual.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),

              //Cuotas R.

              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Cuotas Restantes:',
              //   labelDescription: '16',
              //   fontSize: 16,
              // ),
              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Cuotas Restantes:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.cuotasRestantes.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),

              //Dias de M.

              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Dias de Mora:',
              //   labelDescription: '0',
              //   fontSize: 16,
              // ),

              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Dias de Mora:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.diasMora.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),

              //Interes de M.
              // FilaDatos(
              //   size: size,
              //   labelTittle: 'Interes de Mora:',
              //   labelDescription: '0',
              //   fontSize: 16,
              // ),

              Container(
                width: size.width * 0.8,
                height: size.height * 0.05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //texto 1era columna
                    Text(
                      'Interes de Mora:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Obx(() => Text(
                          getXCobros.interesMora.value,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),

              SizedBox(height: size.height * 0.02),
              Text(
                'Valor cuota crédito',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
              ),
              Obx(
                () => Text(
                  getXCobros.vCuotaMensual.value,
                  style: TextStyle(
                    fontSize: 65,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.03),

              //Continuar
              MyBotton(
                label: 'Continuar',
                labelColor: Colors.white,
                labelSize: 20,
                bottonColor: Color(0xFFD20B12),
                sizeW: size.width * 0.80,
                sizeH: size.height * 0.08,
                fontWeight: FontWeight.bold,
                ontap: () {
                  // c.fillData();
                  // print(c.data.toString());
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ConfirmarCobroPage()));
                },
              ),
              SizedBox(height: size.height * 0.06),
            ],
          ),
        ),
      ),

      //-botton
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NavigationBar()));
        },
        child: Icon(Icons.keyboard_backspace_rounded),
      ),
    );
  }
}
