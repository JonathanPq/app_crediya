import 'package:flutter/material.dart';
import 'package:template_credi/pages/confirmation_pages/confirmar_deposito.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/text_form_field.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class DepositosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              // Logo
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),
              SizedBox(height: size.height * 0.05),

              // Encabezado
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Depositos',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: size.height * 0.02),

              //Numero de cuenta y boton validar
              Container(
                width: size.width * 0.85,
                height: size.height * 0.1,
                child: Row(
                  children: <Widget>[
                    //Input de No. Cuenta
                    Expanded(
                      child: textFromField(
                        sizeH: size.height * 0.08,
                        sizeW: size.width * 0.85,
                        label: "No. Cuenta",
                        inputType: TextInputType.number,
                        password: false,
                        autocorrect: false,
                      ),
                    ),
                    SizedBox(width: size.width * 0.02),

                    //Boton validar
                    MyBotton(
                      label: 'Validar',
                      labelColor: Colors.white,
                      labelSize: 20,
                      bottonColor: Color(0xFFD20B12),
                      sizeW: size.width * 0.30,
                      sizeH: size.height * 0.09,
                      fontWeight: FontWeight.bold,
                      ontap: () {
                        print('Botton validar');
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: size.height * 0.02),

              // Datos - Texto
              Container(
                width: size.width * 0.85,
                height: size.height * 0.13,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  color: Color(0xFFD8D8D8),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10),
                  child: Text(
                    'Nombre: Michael Jonas Rodriguez\nC.I: 1803212381',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),

              //Input de Monto
              textFromField(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.85,
                label: "Monto",
                inputType: TextInputType.number,
                password: false,
                autocorrect: false,
              ),
              SizedBox(height: size.height * 0.02),

              //Input de Descripcion
              textFromField(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.85,
                label: "Descripcion de depósito",
                inputType: TextInputType.text,
                password: false,
                autocorrect: false,
              ),
              SizedBox(height: size.height * 0.03),

              //Botton continuar
              MyBotton(
                label: 'Continuar',
                labelColor: Colors.white,
                labelSize: 28,
                bottonColor: Color(0xFFD20B12),
                sizeW: size.width * 0.7,
                sizeH: size.height * 0.07,
                ontap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ConfirmarDepositoPage()));
                },
              ),
              SizedBox(height: size.height * 0.02),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NavigationBar()));
        },
        child: Icon(Icons.keyboard_backspace_rounded),
      ),
    );
  }
}
