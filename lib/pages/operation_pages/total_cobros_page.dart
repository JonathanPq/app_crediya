import 'package:flutter/material.dart';
import 'package:template_credi/pages/resumen_page.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class TotalCobrosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Medida de la pantalla
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),

              //Logo Crediya
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),

              // Titular
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Total cobros',
                labelColor: Colors.black87,
                labelSize: 30,
                fontWeight: FontWeight.bold,
              ),

              //Monto
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'S-78342',
                labelColor: Colors.black87,
                labelSize: 50,
                fontWeight: FontWeight.bold,
              ),

              //Fecha
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: '26 de julio 2021',
                labelColor: Colors.black87,
                labelSize: 20,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ResumenPage()));
        },
        child: Icon(Icons.keyboard_backspace_rounded),
      ),
    );
  }
}
