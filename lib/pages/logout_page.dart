import 'package:flutter/material.dart';
import 'package:template_credi/pages/login_page.dart';
import 'package:template_credi/widgets/my_bottom.dart';

class LogoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: MyBotton(
          sizeH: size.height * 0.1,
          sizeW: size.width * 0.4,
          label: 'SALIR',
          labelColor: Colors.white,
          bottonColor: Color(0xFFD20B12),
          labelSize: 20,
          ontap: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => LoginPage()));
          },
        ),
      ),
    );
  }
}
