import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:template_credi/controllers/session_controller.dart';
import 'package:template_credi/widgets/Zborrar_background.dart';
import 'package:template_credi/widgets/Zlogin_form.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/text_form_field.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class LoginPage extends StatelessWidget {
  static String screenRoute = '/login';

  var userController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final SessionController getxsesion = Get.put(SessionController());
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),
              TextoW(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.9,
                label: '¡Bienvenido de nuevo!',
                labelSize: 22,
                labelColor: Colors.black45,
              ),
              Container(
                width: 400,
                height: 200,
                child: Image(
                  image: AssetImage(
                    'assets/images/imagen_login.png',
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              // input correo
              textFromField(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.7,
                controller: userController,
                autocorrect: false,
                label: 'Usuario',
                inputType: TextInputType.emailAddress,
                password: false,
              ),
              SizedBox(height: size.height * 0.02),

              // input pass
              textFromField(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.7,
                controller: passwordController,
                autocorrect: false,
                label: 'Contraseña',
                inputType: TextInputType.emailAddress,
                password: true,
              ),
              SizedBox(height: size.height * 0.05),
              //botton ingresar
              MyBotton(
                sizeH: size.height * 0.08,
                sizeW: size.width * 0.8,
                label: 'Ingresar',
                labelColor: Colors.white,
                bottonColor: Color(0xFFD20B12),
                labelSize: 25,
                ontap: () {
                  // login(context);
                  getxsesion.loginConection(
                    name: userController.text.trim(),
                    password: passwordController.text.trim(),
                  );

                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => NavigationBar()));
                },
              ),
              SizedBox(height: size.height * 0.05),
            ],
          ),
        ),
      ),
    );
  }

//http://453e0cf45393.ngrok.io/sesion  - 18/08/2021
  Future<void> login(context) async {
    if (passwordController.text.isNotEmpty && userController.text.isNotEmpty) {
      var response = await http.post(
          Uri.parse('https://e425c83e64ac.ngrok.io/usersApis/login'),
          body: ({
            'email': userController.text.trim(),
            'password': passwordController.text.trim(),
          }));

      String body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);

      if (response.statusCode == 200) {
        print('resultado===${response.statusCode}');
        print(jsonData['status']);
        print(jsonData['nombre']);
        if (jsonData['status'] == 'ok') {
          Navigator.pushReplacementNamed(context, 'home');
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('Ingreso exitoso.')));
        } else {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(jsonData['status'])));
        }
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(jsonData['status'])));
      }
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Ingrese sus credenciales.')));
    }
  }
}
