import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template_credi/controllers/session_controller.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //inicializamos nuestro gtxde cobros
    final SessionController getxsesion = Get.put(SessionController());
    //Medida de la pantalla
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        child: Center(
          heightFactor: 1,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: size.height * 0.05),
                //Logo Crediya
                LogoEncabezado(
                  sizeH: size.height * 0.18,
                  sizeW: size.width * 0.5,
                ),
                //Imagen Home Page
                HomePageImage(size: size),

                TextoW(
                  sizeH: size.height * 0.1,
                  sizeW: size.width * 0.9,
                  label: '¡Hola, bienvenid@!',
                  labelSize: 30,
                  labelColor: Colors.black87,
                ),

                // TextoW(
                //   sizeH: size.height * 0.1,
                //   sizeW: size.width * 0.9,
                //   label: 'Diego Rodriguez',
                //   labelSize: 35,
                //   labelColor: Colors.black87,
                //   fontWeight: FontWeight.bold,
                // ),

                Container(
                    alignment: Alignment.center,
                    height: size.height * 0.15,
                    width: size.width * 0.9,
                    child: Obx(() => Text(
                          getxsesion.name.string,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                          ),
                        ))),
                SizedBox(height: size.height * 0.05),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class HomePageImage extends StatelessWidget {
  const HomePageImage({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/imagen_home.png'),
          fit: BoxFit.contain,
        ),
      ),
      height: size.height * 0.35,
      width: size.width * 0.75,
    );
  }
}
