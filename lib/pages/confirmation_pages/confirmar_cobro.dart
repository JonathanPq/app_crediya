import 'package:flutter/material.dart';
import 'package:template_credi/pages/confirmation_pages/cobro_exitoso.dart';
import 'package:template_credi/pages/confirmation_pages/deposito_exitoso.dart';
import 'package:template_credi/pages/operation_pages/cobros_page.dart';
import 'package:template_credi/pages/operation_pages/depositos_page.dart';
import 'package:template_credi/widgets/datos_cobros.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class ConfirmarCobroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              // Logo
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),

              // Encabezado
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Informacion socio',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: size.height * 0.02),

              FilaDatos(
                size: size,
                labelTittle: 'Monto:',
                labelDescription: 'S-100',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Nombre:',
                labelDescription: 'Michael Rodriguez',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Nro. cuenta:',
                labelDescription: '176237612',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Cedula:',
                labelDescription: '180123128',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Fecha:',
                labelDescription: '23 Julio 2021',
                fontSize: 16,
              ),
              SizedBox(height: size.height * 0.02),
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Si los datos son correctos \nConfirme el cobro',
                labelSize: 23,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: size.height * 0.02),
              //visto verde
              Center(
                child: Container(
                  height: size.height * 0.15,
                  width: size.width * 0.30,
                  child: Image(
                    fit: BoxFit.contain,
                    image: AssetImage('assets/images/vistoverde.png'),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),

              //botones
              MyBotton(
                  sizeW: size.width * 0.85,
                  sizeH: size.height * 0.08,
                  label: 'Confirmar cobro',
                  labelColor: Colors.white,
                  bottonColor: Color(0xFF00B000),
                  labelSize: 25,
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CobroExitosoPage()));
                  }),
              SizedBox(height: size.height * 0.03),
              MyBotton(
                  sizeW: size.width * 0.85,
                  sizeH: size.height * 0.08,
                  label: 'Regresar',
                  labelColor: Colors.white,
                  bottonColor: Color(0xFF7F7F7F),
                  labelSize: 25,
                  ontap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CobrosPage()));
                  }),
              SizedBox(height: size.height * 0.05),
            ],
          ),
        ),
      ),
    );
  }
}
