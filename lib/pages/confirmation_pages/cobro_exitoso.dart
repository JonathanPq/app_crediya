import 'package:flutter/material.dart';
import 'package:template_credi/widgets/datos_cobros.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class CobroExitosoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              // Logo
              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),

              // Encabezado
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Cobro exitoso',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: size.height * 0.02),
              Padding(
                padding: const EdgeInsets.only(right: 150.0),
                child: Text(
                  'Informacion socio',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
              ),
              SizedBox(height: size.height * 0.02),

              // Informacion de credito
              //Nombre
              FilaDatos(
                size: size,
                labelTittle: 'Nombre:',
                labelDescription: 'Michael Rodriguez',
                fontSize: 16,
              ),
              //Saldo C.
              FilaDatos(
                size: size,
                labelTittle: 'Nro. cuenta:',
                labelDescription: '23784264',
                fontSize: 16,
              ),
              //Valor C.
              FilaDatos(
                size: size,
                labelTittle: 'Cedula:',
                labelDescription: '101892381',
                fontSize: 16,
              ),
              //Cuotas R.
              FilaDatos(
                size: size,
                labelTittle: 'Monto:',
                labelDescription: 'S-3216',
                fontSize: 16,
              ),
              //Dias de M.
              FilaDatos(
                size: size,
                labelTittle: 'Fecha deposito:',
                labelDescription: '23 julio 2021',
                fontSize: 16,
              ),
              SizedBox(height: size.height * 0.02),
              Padding(
                padding: const EdgeInsets.only(right: 220.0),
                child: Text(
                  'Comentario',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              Container(
                color: Colors.black26,
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40.0),
                  child: Text(
                    'Pago de cuota recaudado en mercado modelo S-2342',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Text(
                    'Informacion de credito',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              FilaDatos(
                size: size,
                labelTittle: 'Saldo credito:',
                labelDescription: 'S-3216',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Cuotas restantes:',
                labelDescription: '15',
                fontSize: 16,
              ),
              FilaDatos(
                size: size,
                labelTittle: 'Prox. fecha pago:',
                labelDescription: '23 agosto 2021',
                fontSize: 16,
              ),
              SizedBox(height: size.height * 0.02),

              MyBotton(
                  sizeW: size.width * 0.85,
                  sizeH: size.height * 0.08,
                  label: 'Imprimir',
                  labelColor: Colors.white,
                  bottonColor: Color(0xFF00B000),
                  labelSize: 25,
                  ontap: () {}),
              SizedBox(height: size.height * 0.03),
              MyBotton(
                  sizeW: size.width * 0.85,
                  sizeH: size.height * 0.08,
                  label: 'Regresar al menu',
                  labelColor: Colors.white,
                  bottonColor: Color(0xFF7F7F7F),
                  labelSize: 25,
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NavigationBar()));
                  }),
              SizedBox(height: size.height * 0.05),
            ],
          ),
        ),
      ),
    );
  }
}
