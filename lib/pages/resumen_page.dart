import 'package:flutter/material.dart';
import 'package:template_credi/pages/operation_pages/cobros_page.dart';
import 'package:template_credi/pages/operation_pages/total_cobros_page.dart';
import 'package:template_credi/pages/operation_pages/total_depositos_page.dart';
import 'package:template_credi/widgets/navigation_bar.dart';
import 'package:template_credi/widgets/tarjeta_resumen.dart';
import 'package:template_credi/widgets/logo_encabezado.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class ResumenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Medida de la pantalla
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        heightFactor: 1,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),

              LogoEncabezado(
                sizeH: size.height * 0.18,
                sizeW: size.width * 0.5,
              ),
              // Texto
              TextoW(
                sizeH: size.height * 0.1,
                sizeW: size.width * 0.9,
                label: 'Total Recaudación',
                labelSize: 30,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),

              TextoW(
                sizeH: size.height * 0.15,
                sizeW: size.width * 0.9,
                label: 'S-78342',
                labelSize: 50,
                labelColor: Colors.black87,
                fontWeight: FontWeight.bold,
              ),

              //Tarjeta de deposito
              TarjetaResumen(
                assetLogoImage: 'assets/images/deposito_blanco.png',
                cardColor: Color(0xFFD20B12),
                label: 'Depósitos',
                labelSize: 25,
                secondLabel: 'S-1231',
                secondLabelSize: 35,
                labelColor: Colors.white,
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TotalDespositosPage()));
                },
              ),
              SizedBox(height: size.height * 0.03),

              //-Tarjeta de cobros
              TarjetaResumen(
                assetLogoImage: 'assets/images/cobro_gris.png',
                cardColor: Colors.white,
                label: 'Cobros',
                labelSize: 25,
                secondLabel: 'S-1232',
                secondLabelSize: 35,
                labelColor: Color(0xFF3F3F3F),
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TotalCobrosPage()));
                },
              ),
              SizedBox(height: size.height * 0.05),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NavigationBar()));
        },
        child: Icon(Icons.keyboard_backspace_rounded),
      ),
    );
  }
}
