import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template_credi/pages/actividad_nueva_page.dart';
import 'package:template_credi/pages/home_page.dart';
import 'package:template_credi/pages/login_page.dart';
import 'package:template_credi/pages/logout_page.dart';
import 'package:template_credi/pages/operation_pages/cobros_page.dart';
import 'package:template_credi/pages/operation_pages/depositos_page.dart';
import 'package:template_credi/pages/operation_pages/total_depositos_page.dart';
import 'package:template_credi/pages/resumen_page.dart';
import 'package:template_credi/widgets/navigation_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Montserrat'),
      title: 'Template App',
      home: LoginPage(),
    );
  }
}
