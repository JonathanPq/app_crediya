import 'package:flutter/material.dart';

class TarjetaResumen extends StatelessWidget {
  final String label;
  final double labelSize;
  final String secondLabel;
  final double secondLabelSize;
  final Color labelColor;
  final Color cardColor;
  final String assetLogoImage;
  final Function onTap;

  const TarjetaResumen({
    Key? key,
    required this.label,
    required this.secondLabel,
    required this.labelSize,
    required this.secondLabelSize,
    required this.labelColor,
    required this.cardColor,
    required this.assetLogoImage,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(15.0)),
      onTap: () {
        onTap();
      },
      child: ItemCardResumen(
        image: assetLogoImage,
        title: label,
        tittleSize: labelSize,
        subtitle: secondLabel,
        subtitleSize: secondLabelSize,
        color: cardColor,
        labelColor: labelColor,
      ),
    );
  }
}

class ItemCardResumen extends StatelessWidget {
  String image, title, subtitle;
  Color color;
  Color labelColor;
  double tittleSize;
  double subtitleSize;
  ItemCardResumen({
    required this.image,
    required this.title,
    required this.tittleSize,
    required this.subtitleSize,
    required this.subtitle,
    required this.color,
    required this.labelColor,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.15,
      width: size.width * 0.75,
      decoration: cardDecoration(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Logo - operacion
              Container(
                height: size.height * 0.13,
                width: size.width * 0.13,
                child: Image(
                  image: AssetImage(image),
                ),
              ),
              SizedBox(width: size.width * 0.03),
              //Columna de textos
              Column(
                children: [
                  //Label titulo
                  Text(
                    title,
                    style: TextStyle(
                      color: labelColor,
                      fontSize: tittleSize,
                    ),
                  ),
                  //Label subtitulo

                  Text(
                    subtitle,
                    style: TextStyle(
                      color: labelColor,
                      fontSize: subtitleSize,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration cardDecoration() {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 2,
          blurRadius: 2,
          offset: Offset(0, 1), // changes position of shadow
        ),
      ],
      color: color,
      borderRadius: BorderRadius.all(
        Radius.circular(20.0),
      ),
    );
  }
}
