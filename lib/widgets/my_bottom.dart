import 'package:flutter/material.dart';

class MyBotton extends StatelessWidget {
  final Function ontap;
  final String label;
  final Color labelColor;
  final Color bottonColor;
  final double labelSize;
  final double sizeW;
  final double sizeH;
  final FontWeight? fontWeight;

  const MyBotton({
    Key? key,
    required this.sizeW,
    required this.sizeH,
    required this.label,
    required this.labelColor,
    required this.bottonColor,
    required this.labelSize,
    this.fontWeight,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: sizeW,
      height: sizeH,
      color: Colors.white,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: bottonColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        onPressed: () {
          ontap();
        },
        child: Text(
          label,
          style: TextStyle(
            color: labelColor,
            fontSize: labelSize,
            fontWeight: fontWeight,
          ),
        ),
      ),
    );
  }
}
