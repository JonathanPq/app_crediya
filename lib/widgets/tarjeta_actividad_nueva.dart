import 'package:flutter/material.dart';

class TarjetaActividadNueva extends StatelessWidget {
  final String label;
  final Color labelColor;
  final Color cardColor;
  final String assetLogoImage;
  final Function onTap;

  const TarjetaActividadNueva({
    Key? key,
    required this.label,
    required this.labelColor,
    required this.cardColor,
    required this.assetLogoImage,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(15.0)),
      onTap: () {
        onTap();
      },
      child: ItemCard(
        image: assetLogoImage,
        title: label,
        color: cardColor,
        labelColor: labelColor,
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  String image, title;
  Color color;
  Color labelColor;
  ItemCard({
    required this.image,
    required this.title,
    required this.color,
    required this.labelColor,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding:
          const EdgeInsets.only(left: 10.0, right: 10.0, top: 8.0, bottom: 5.0),
      child: Container(
        height: size.height * 0.30,
        width: size.height * 0.35,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ], color: color, borderRadius: BorderRadius.all(Radius.circular(15.0))),
        child: Column(
          children: [
            SizedBox(height: size.height * 0.02),
            Container(
              height: size.height * 0.20,
              width: size.width * 0.20,
              child: Image(
                image: AssetImage(image),
              ),
            ),
            Text(
              title,
              style: TextStyle(
                fontSize: 27,
                color: labelColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
