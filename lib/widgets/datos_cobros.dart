import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template_credi/controllers/cobros_controller.dart';

import 'package:template_credi/widgets/my_bottom.dart';
import 'package:template_credi/widgets/texto_encabezado.dart';

class DatosCobros extends StatelessWidget {
  // const DatosCobros({
  //   Key? key,
  //   required this.size,
  // }) : super(key: key);

  // final Size size = 0;
  final data = {"complete": 0, "mora": 0};
  final bool flagData = false;
  @override
  Widget build(BuildContext context) {
    print('data');
    print(data);
    // if (data['complete'] == 0) {
    //   return Container(
    //     child: Text("No hay Data"),
    //   );
    // } else {
    //   return Container(
    //     child: Text(data['mora'].toString()),
    //   );
    // }
    return Container();
    // return Container(
    //   child: Column(
    //     children: [
    //       // Datos - Texto
    //       Container(
    //         width: size.width * 0.85,
    //         height: size.height * 0.1,
    //         decoration: BoxDecoration(
    //           borderRadius: BorderRadius.circular(6.0),
    //           color: Color(0xFFD8D8D8),
    //         ),
    //         child: Padding(
    //           padding: const EdgeInsets.only(left: 20, top: 10),
    //           child: Text(
    //             'Nombre: Michael Jonas Rodriguez\nC.I: 1803212381',
    //             style: TextStyle(
    //               fontSize: 18,
    //             ),
    //             textAlign: TextAlign.start,
    //           ),
    //         ),
    //       ),

    //       //texto
    //       TextoW(
    //         sizeH: size.height * 0.15,
    //         sizeW: size.width * 0.9,
    //         label: 'Información de crédito',
    //         labelSize: 31,
    //         labelColor: Colors.black87,
    //         fontWeight: FontWeight.bold,
    //       ),

    //       //Informacion de credito
    //       //Tipo C.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Tipo de credito:',
    //         labelDescription: 'MicroCrédito',
    //         fontSize: 16,
    //       ),
    //       //Saldo C.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Saldo de credito:',
    //         labelDescription: 'S-1823',
    //         fontSize: 16,
    //       ),
    //       //Valor C.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Valor de cuota mensual:',
    //         labelDescription: 'S-18123',
    //         fontSize: 16,
    //       ),
    //       //Cuotas R.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Cuotas Restantes:',
    //         labelDescription: '16',
    //         fontSize: 16,
    //       ),
    //       //Dias de M.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Dias de Mora:',
    //         labelDescription: '0',
    //         fontSize: 16,
    //       ),
    //       //Interes de M.
    //       FilaDatosCredito(
    //         size: size,
    //         labelTittle: 'Interes de Mora:',
    //         labelDescription: '0',
    //         fontSize: 16,
    //       ),
    //       SizedBox(height: size.height * 0.02),
    //       Text(
    //         'Valor cuota crédito',
    //         style: TextStyle(
    //           fontSize: 25,
    //           fontWeight: FontWeight.bold,
    //           color: Colors.black87,
    //         ),
    //       ),
    //       Text(
    //         'S-1271',
    //         style: TextStyle(
    //           fontSize: 65,
    //           fontWeight: FontWeight.bold,
    //           color: Colors.black87,
    //         ),
    //       ),
    //       SizedBox(height: size.height * 0.03),

    //       //Botton continuar
    //       MyBotton(
    //         label: 'Continuar',
    //         labelColor: Colors.white,
    //         labelSize: 28,
    //         bottonColor: Color(0xFFD20B12),
    //         sizeW: size.width * 0.7,
    //         sizeH: size.height * 0.07,
    //         ontap: () {
    //           print('Botton continuar');
    //         },
    //       ),
    //     ],
    //   ),
    // );
  }
}

class FilaDatos extends StatelessWidget {
  final String labelTittle;
  final String labelDescription;
  final double fontSize;
  final FontWeight? tittleFontWeight;
  const FilaDatos({
    Key? key,
    required this.size,
    required this.labelTittle,
    required this.labelDescription,
    required this.fontSize,
    this.tittleFontWeight,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.width * 0.8,
      height: size.height * 0.05,
      color: Colors.black26,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //texto 1era columna
          Text(
            labelTittle,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: fontSize,
            ),
          ),

          Text(
            labelDescription,
            style: TextStyle(
              fontSize: fontSize,
            ),
          )
        ],
      ),
    );
  }
}
