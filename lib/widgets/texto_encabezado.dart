import 'package:flutter/material.dart';

class TextoW extends StatelessWidget {
  final double sizeW;
  final double sizeH;
  final String label;
  final double labelSize;
  final Color labelColor;
  final FontWeight? fontWeight;

  const TextoW({
    required this.sizeW,
    required this.sizeH,
    required this.label,
    required this.labelSize,
    required this.labelColor,
    this.fontWeight,
  });

  @override
  Widget build(BuildContext context) {

    return Container(
      alignment: Alignment.center,
      
      height: sizeH,
      width: sizeW,
      child: Text(
        label,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: TextStyle(
          fontSize: labelSize,
          color: labelColor,
          fontWeight: fontWeight,
        ),
      ),
    );
  }
}
