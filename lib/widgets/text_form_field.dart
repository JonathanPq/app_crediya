import 'package:flutter/material.dart';

class textFromField extends StatelessWidget {
  final double sizeW;
  final double sizeH;
  String label;
  TextInputType inputType;
  final bool password;
  final bool autocorrect;
  var controller;

  textFromField({
    required this.sizeW,
    required this.sizeH,
    required this.label,
    required this.inputType,
    required this.password,
    required this.autocorrect,
    var this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: size.height * 0.08,
      // width: size.width * 0.85,
      height: sizeH,
      width: sizeW,
      alignment: AlignmentDirectional.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
        color: Colors.white,
        boxShadow: [BoxShadow(spreadRadius: 1, color: Colors.black45)],
      ),
      child: Theme(
        data: ThemeData(
          fontFamily: 'Montserrat',
          hintColor: Colors.transparent,
        ),
        child: TextFormField(
          controller: controller,
          autocorrect: autocorrect,
          obscureText: password,
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: label,
            icon: Icon(
              Icons.ac_unit,
              size: 10,
              color: Colors.transparent,
            ),
            labelStyle: TextStyle(
              fontSize: 15.0,
              letterSpacing: 0.3,
              color: Colors.black87,
            ),
          ),
          keyboardType: inputType,
        ),
      ),
    );
  }
}
