import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'package:template_credi/pages/actividad_nueva_page.dart';
import 'package:template_credi/pages/home_page.dart';
import 'package:template_credi/pages/logout_page.dart';
import 'package:template_credi/pages/resumen_page.dart';

class NavigationBar extends StatefulWidget {
  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  int currentIndex = 0;

  /// Set a type current number a layout class
  Widget llamadaPagina(int current) {
    switch (current) {
      case 0:
        return HomePage(); 
      case 1:
        return ActividadPage(); //new
      case 2:
        return ResumenPage();
      case 3:
        return LogoutPage();
      default:
        return HomePage();
    }
  }

  /// Build BottomNavigationBar Widget
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: llamadaPagina(currentIndex),
      bottomNavigationBar: CurvedNavigationBar(
        color: Color(0xFFD20B12),
        backgroundColor: Colors.white,
        items: <Widget>[
          Icon(
            Icons.home,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.add,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.article_outlined,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.logout,
            size: 30,
            color: Colors.white,
          ),
        ],
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
      ),
    );
  }
}
