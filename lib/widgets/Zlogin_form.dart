import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:template_credi/decoration/login_input_decoration.dart';
import 'package:template_credi/widgets/my_bottom.dart';

// ignore: must_be_immutable
class LoginForm extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            //Input de correo
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(spreadRadius: 0.7, color: Colors.grey.shade400)
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: TextFormField(
                  controller: emailController,
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.myDecoration(
                    labelText: 'Correo Electronico',
                    hintText: 'myexaple123@gmail.com',
                  ),
                  // validator: (value) {
                  //   String pattern =
                  //       r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  //   RegExp regExp = new RegExp(pattern);
                  //   return regExp.hasMatch(value ?? '')
                  //       ? null
                  //       : 'Ingrese un correo electronico valido.';
                  // },
                ),
              ),
            ),
            SizedBox(height: 30),
            //Input de contraseña
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(spreadRadius: 0.7, color: Colors.grey.shade400)
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: TextFormField(
                  controller: passwordController,
                  autocorrect: false,
                  obscureText: true,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.myDecoration(
                    labelText: 'Contraseña',
                    hintText: '...........',
                  ),
                  // validator: (value) {
                  //   return (value != null && value.length >= 8)
                  //       ? null
                  //       : 'La contraseña debe ser de 8 caracteres o más. ';
                  // },
                ),
              ),
            ),
            SizedBox(height: size.height * 0.05),
            //Boton de ingresar

            MyBotton(
              sizeW: size.width * 0.9,
              sizeH: size.height * 0.08,
              label: 'Ingresar',
              labelColor: Colors.white,
              bottonColor: Color(0xFFD20B12),
              labelSize: 18,
              ontap: () {
                login(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  // Solicitud
  Future<void> login(context) async {
    if (passwordController.text.isNotEmpty && emailController.text.isNotEmpty) {
      var response = await http.post(
          Uri.parse('https://e425c83e64ac.ngrok.io/usersApis/login'),
          body: ({
            'email': emailController.text.trim(),
            'password': passwordController.text.trim(),
          }));

      String body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);

      if (response.statusCode == 200) {
        print('resultado===${response.statusCode}');
        print(jsonData['status']);
        print(jsonData['nombre']);
        if (jsonData['status'] == 'ok') {
          Navigator.pushReplacementNamed(context, 'home');
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('Ingreso exitoso.')));
        } else {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(jsonData['status'])));
        }
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(jsonData['status'])));
      }
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Ingrese sus credenciales.')));
    }
  }
}
