import 'package:flutter/material.dart';

class LogoEncabezado extends StatelessWidget {
  final double sizeW;
  final double sizeH;
  const LogoEncabezado({
    required,
    required this.sizeW,
    required this.sizeH,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //TODO: sizeW, sizeH
      width: sizeW,
      height: sizeH,
      child: Image(
        image: AssetImage('assets/images/logo_login.png'),
      ),
    );
  }
}
